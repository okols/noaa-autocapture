#!/bin/bash

#This script is placed in the same directory as the config files for Predict, usually where the below 'dir' is.
#This very simple script is mainly meant to work for downloading NOAA TLEs, but can be modified for other satellites as well. Simply
#check out celestrak.com to find the URL for your satellites of interest.

#Usage
#To work with NOAA satellites only, first delete any TLE files in the dir .predict. Then run this script to create the
#NOAA TLEs. Now run Predict and input your location. Since the NOAA TLEs are the only available, you shouldn't need to
#do anything more with it.
#Ideally this script should be ran every day as long as this automatic process is running. The groundtrack of satellites
#can change quite a bit over relatively short time, so having updated TLEs is crucial for this whole process to work.

#---kols


dir='/home/your_home_dir/.predict/'

wget -q https://www.celestrak.com/NORAD/elements/noaa.txt -O $dir/tmp.txt

if [ $? -eq 0 ];
  then
    cat $dir/tmp.txt | sed 's/[[].*//' > $dir/predict.tle
    rm $dir/tmp.txt
fi
