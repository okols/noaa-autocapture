#!/usr/bin/env python
# -*- coding: utf-8 -*-

#This script will use pypredict to predict when a passing satellite (chosen in 'satellites' variable), record the data,
#and decode to produce a map.

#Unfortunately, I've forgotten who originally made it, so all I can say is that while it's heavily modified by me, 
#it was not originally designed by yours truly.
#--kols

#Search and replace 'your_home_dir' with your actual home directory

#Dependencies:

#--All the imported modules
#--Custom module 'pypredict' is dependent on the package 'predict', availible for most distros
#--Predict is dependent on updated Two Line Element orbital data for the chosen satellites, which can be obtained by
#the script 'tle_update'. This script is placed in your home directory, under /.predict/. Ideally this script should be
#every day as long as this automatic process is running. 
#--WxtoImg (There is a version for RPi, but any version will do)


import time
import datetime
from time import gmtime, strftime
import pypredict
import subprocess
import os

#------------------------------
#------DEBUGGING---------------
#------------------------------
#Setting this parameter to 'yes'
#is only for testing and will not
#produce any meaningful data
debugging = "no"

# Minimum elevation for a pass
min_elev = 25

# Satellite names in TLE plus their frequency
satellites = ['NOAA 18','NOAA 15','NOAA 19']
freqs = [137912500, 137620000, 137100000]

# Dongle gain
dongleGain='50'

# Dongle PPM shift, hopefully this will change to reflect different PPM on freq
dongleShift='-3'

# Dongle index, is there any rtl_fm allowing passing serial of dongle? Unused right now
dongleIndex='0'

# Sample rate, width of recorded signal - should include few kHz for doppler shift
sample ='60000'
# Sample rate of the wav file. Shouldn't be changed
wavrate='11025'

# Should I remove RAWs?
removeRAW='yes'

# Directories used in this program
# wxtoimg install dir
wxInstallDir='/usr/local/bin'
# Recording dir, used for RAW and WAV files
recdir='/home/your_home_dir/rtlsdr/wxtoimg/audio'

# Spectrogram directory, this would be optional in the future
specdir='/home/your_home_dir/rtlsdr/wxtoimg/spectro'
  
# Output image directory
imgdir='/home/your_home_dir/rtlsdr/wxtoimg/img'
# Map file directory
mapDir='/home/your_home_dir/rtlsdr/wxtoimg/maps'

# Options for wxtoimg
# Create map overlay?
wxAddOverlay='yes'
# Image outputs
wxEnhHVC='yes'
wxEnhHVCT='no'
wxEnhMSA='no'
wxEnhMCIR='no'
# Other tunables
wxQuietOutput='no'
wxDecodeAll='yes'
wxJPEGQuality='100'
# Adding overlay text
wxAddTextOverlay='yes'
wxOverlayText=''

# Various options
# Should this script create spectrogram : yes/no
createSpectro='yes'
# Use doppler shift for correction, not used right now - leave as is
runDoppler='no'

# Read qth file for station data
stationFileDir=os.path.expanduser('~')
stationFilex=stationFileDir+'/.predict/predict.qth'
stationFile=open(stationFilex, 'r')
stationData=stationFile.readlines()
stationName=str(stationData[0]).rstrip().strip()
stationLat=str(stationData[1]).rstrip().strip()
stationLon=str(stationData[2]).rstrip().strip()
stationAlt=str(stationData[3]).rstrip().strip()
stationFile.close()

stationLonNeg=float(stationLon)*-1

#Creating options for wxtoimg
if wxQuietOutput in ('yes', 'y', '1'):
    wxQuietOpt='-q'
else:
    wxQuietOpt='-C wxQuiet:no'

if wxDecodeAll in ('yes', 'y', '1'):
    wxDecodeOpt='-A'
else:
    wxDecodeOpt='-C wxDecodeAll:no'

if wxAddTextOverlay in ('yes', 'y', '1'):
    wxAddText='-k '+wxOverlayText
else:
    wxAddText='-C wxOther:noOverlay'

#Executes recording command
def runForDuration(cmdline, duration):
    try:
        child = subprocess.Popen(cmdline)
        time.sleep(duration)
        print "\nWaiting for rtl_fm termination..."
        child.terminate()
        time.sleep(10)
    except OSError as e:
        print "OS Error during command: "+" ".join(cmdline)
        print "OS Error: "+e.strerror

#Mainly for debug, calculates length of recording
def actual_length(fname):
    cmd = ['sox', recdir+'/'+fname+'.wav', '-n', 'stat']
    process = subprocess.Popen(cmd, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    out, err = process.communicate()
    for i in err.splitlines():
      if i.startswith("Length"):
        length = int(float(i.split()[2]))
    return length

#Initiates recording and transcoding
def recordWAV(freq,fname,duration):
    recordFM(freq,fname,duration,xfname)
    transcode(fname)
    actual_recordTime = actual_length(fname)
    difference = actual_recordTime - duration
    print "Record time: " +str(duration)
    print "Actual record time: " +str(actual_recordTime)
    print "Difference in record time: " +str(difference)
    if createSpectro in ('yes', 'y', '1'):
	    spectrum(fname)

#Sets up recording parameters
def recordFM(freq, fname, duration, xfname):
    cmdline = ['rtl_fm',\
		'-f',str(freq),\
		'-s',sample,\
		#'-g',dongleGain,\
		'-F','9',\
		#'-A','fast',\
		#'-o','4',\
		'-E','deemp',\
        	'-E', 'wav',\
		#'-E','offset',\
		'-p',dongleShift,\
		recdir+'/'+fname+'.raw']
    runForDuration(cmdline, duration)

#Executes transcoding of recording
def transcode(fname):
    print '\nTranscoding...'
    cmdline = ['sox','-t','wav','-es','-b','16','-c','1','-V1',recdir+'/'+fname+'.raw',recdir+'/'+fname+'.wav','rate',wavrate]
    subprocess.call(cmdline)
    #if removeRAW in ('yes', 'y', '1'):
    os.remove(recdir+'/'+fname+'.raw')

#Applies freq change to account for doppler
def doppler(fname,emergeTime):
    cmdline = ['doppler', 
    '-d','',\
    '--tlefile', '~/.predict/predict.tle',\
    '--tlename', xfname,\
    '--location', 'lat='+stationLat+',lon='+stationLon+',alt='+stationAlt,\
    '--freq ', +str(freq),\
    '-i', 'i16',\
    '-s', sample ]
    subprocess.call(cmdline)

#Creates map overlay
def createoverlay(fname,aosTime_wxmap,satName):
    print '\nCreating Map Overlay...'
    cmdline = ['wxmap',
    '-T',satName,\
    '-G',stationFileDir+'/.predict/',\
    '-H','predict.tle',\
    '-M','0',\
    '-L',stationLat+'/'+str(stationLonNeg)+'/'+stationAlt,\
    '-a',str(aosTime_wxmap), mapDir+'/'+str(fname)+'-map.png'] #'-a',str(aosTime_wxmap)
    subprocess.call(cmdline)

#def createoverlay(fname,aosTime_wxmap,length,satName):
#    print 'Creating Map Overlay...'
#    cmdline = ['wxmap',
#    '-T',satName,\
#    '-G',stationFileDir+'/.predict/',\
#    '-H','predict.tle',\
#    '-M','0',\
#    '-L',stationLat+'/'+str(stationLonNeg)+'/'+stationAlt,\
#    '-O',str(length), str(aosTime_wxmap), mapDir+'/'+str(fname)+'-map.png']
#    subprocess.call(cmdline)

#Creates the folder in which to store images, based on current date
def create_date_dir():
    dir_date = time.strftime("%d_%m_%y")
    dir_date_path = imgdir+'/'+dir_date

    if not os.path.isdir(dir_date_path):
     os.makedirs(dir_date_path)

    #FNULL=open(os.devnull, 'w')
    #subprocess.call('mkdir %s ' % dir_date ,close_fds=True, stdout=FNULL, stderr=subprocess.STDOUT, shell=True)
    
    return dir_date

#Decoding wav to image
#This should be a class, and each enchancement should be a function
def decode(fname,aosTime,satName):
    print "\nDecoding image..."
    satTimestamp = int(fname)
    fileNameC = datetime.datetime.fromtimestamp(satTimestamp).strftime('%Y%m%d-%H%M')
    
    dir_date=create_date_dir()

    wxAddText='-k.' #"-k." adds default wxtoimg information
   
    force_sat = str(satName.replace(' ','-')) #this forces wx to decode current satellite

    #First, create raw image
    print 'Creating raw image'
    cmdline = [ wxInstallDir+'/wxtoimg','-16r','-t'+force_sat,
              recdir+'/'+fname+'.wav',
              imgdir+'/'+dir_date+'/'+satName+'/'+fileNameC+'-raw.png']
    subprocess.call(cmdline)
    
    #Determining whether or not the map overlay will be used
    #This block uses the map overlay
    if wxAddOverlay in ('yes', 'y', '1'):
      print 'Creating normal image with map'
      length = actual_length(fname)
      aosTime_wxmap = time.strftime("%d %b %Y %H:%M:%S", time.gmtime(int(aosTime)))
      createoverlay(fname,aosTime_wxmap,satName)
      cmdline = [ wxInstallDir+'/wxtoimg','-t'+force_sat,wxQuietOpt,wxDecodeOpt,wxAddText,
                '-Q '+wxJPEGQuality,'-m', mapDir+'/'+fname+'-map.png',recdir+'/'+fname+'.wav',
                imgdir+'/'+dir_date+'/'+satName+'/'+fileNameC+'-normal.jpg']
      subprocess.call(cmdline)
    
      if wxEnhHVC in ('yes', 'y', '1'):
        print 'Creating HVC image with map'
        cmdline_hvc = [ wxInstallDir+'/wxtoimg','-t'+force_sat,wxQuietOpt,wxDecodeOpt,wxAddText,'-Q '+wxJPEGQuality,
                      '-e','HVC','-m',mapDir+'/'+fname+'-map.png',recdir+'/'+fname+'.wav', 
                      imgdir+'/'+dir_date+'/'+satName+'/'+fileNameC+'-hvc.jpg']
        subprocess.call(cmdline_hvc)
      
      if wxEnhHVCT in ('yes', 'y', '1'):
        print 'Creating HVCT image with map'
        cmdline_hvct = [ wxInstallDir+'/wxtoimg','-t'+force_sat,wxQuietOpt,wxDecodeOpt,wxAddText,'-Q '+wxJPEGQuality,
                       '-e','HVCT','-m',mapDir+'/'+fname+'-map.png',recdir+'/'+fname+'.wav',
                       imgdir+'/'+dir_date+'/'+satName+'/'+fileNameC+'-hvct.jpg']
        subprocess.call(cmdline_hvct)
      
      if wxEnhMSA in ('yes', 'y', '1'):
        print 'Creating MSA image with map'
        cmdline_msa_prep = [ wxInstallDir+'/wxtoimg','-t'+force_sat,wxQuietOpt,wxDecodeOpt,wxAddText,'-Q '+wxJPEGQuality,
                           '-e','MSA-precip','-m',mapDir+'/'+fname+'-map.png',recdir+'/'+fname+'.wav',
                           imgdir+'/'+dir_date+'/'+satName+'/'+fileNameC+'-msa_prep.jpg']
        subprocess.call(cmdline_msa_prep)
        cmdline_msa = [ wxInstallDir+'/wxtoimg','-t'+force_sat,wxQuietOpt,wxDecodeOpt,wxAddText,'-Q '+wxJPEGQuality,
                      '-e','MSA-precip','-m',mapDir+'/'+fname+'-map.png',recdir+'/'+fname+'.wav',
                      imgdir+'/'+dir_date+'/'+satName+'/'+fileNameC+'-msa.jpg']
        subprocess.call(cmdline_msa)
      
      if wxEnhMCIR in ('yes', 'y', '1'):
        print 'Creating MCIR image with map'
        cmdline_mcir = [ wxInstallDir+'/wxtoimg','-t'+force_sat,wxQuietOpt,wxDecodeOpt,wxAddText,'-Q '+wxJPEGQuality,
                       '-e','MCIR','-m',mapDir+'/'+fname+'-map.png',recdir+'/'+fname+'.wav',
                       imgdir+'/'+dir_date+'/'+satName+'/'+fileNameC+'-mcir.jpg']
        subprocess.call(cmdline_mcir)
    
    #This block does not use the map overlay
    else:
      print 'Creating normal image without map'
      cmdline = [ wxInstallDir+'/wxtoimg','-t'+force_sat,wxQuietOpt,wxDecodeOpt,wxAddText,'-Q '+wxJPEGQuality,
                recdir+'/'+fname+'.wav', imgdir+'/'+dir_date+'/'+satName+'/'+fileNameC+'-normal.jpg']
      subprocess.call(cmdline)
    
      if wxEnhHVC in ('yes', 'y', '1'):
        print 'Creating HVC image without map'
        cmdline_hvc = [ wxInstallDir+'/wxtoimg','-t'+force_sat,wxQuietOpt,wxDecodeOpt,wxAddText,'-Q '+wxJPEGQuality,
                      '-e','HVC',recdir+'/'+fname+'.wav', 
                      imgdir+'/'+dir_date+'/'+satName+'/'+fileNameC+'-hvc.jpg']
        subprocess.call(cmdline_hvc)
      
      if wxEnhHVCT in ('yes', 'y', '1'):
        print 'Creating HVCT image without map'
        cmdline_hvct = [ wxInstallDir+'/wxtoimg','-t'+force_sat,wxQuietOpt,wxDecodeOpt,wxAddText,'-Q '+wxJPEGQuality,
                       '-e','HVCT',recdir+'/'+fname+'.wav', 
                       imgdir+'/'+dir_date+'/'+satName+'/'+fileNameC+'-hvct.jpg']
        subprocess.call(cmdline_hvct)
      
      if wxEnhMSA in ('yes', 'y', '1'):
        print 'Creating MSA image without map'
        cmdline_msa = [ wxInstallDir+'/wxtoimg','-t'+force_sat,wxQuietOpt,wxDecodeOpt,wxAddText,'-Q '+wxJPEGQuality,
                      '-e','MSA',recdir+'/'+fname+'.wav', 
                      imgdir+'/'+dir_date+'/'+satName+'/'+fileNameC+'-msa.jpg']
        subprocess.call(cmdline_msa)
      
      if wxEnhMCIR in ('yes', 'y', '1'):
        print 'Creating MCIR image without map'
        cmdline_mcir = [ wxInstallDir+'/wxtoimg','-t'+force_sat,wxQuietOpt,wxDecodeOpt,wxAddText,'-Q '+wxJPEGQuality,
                        '-e','MCIR',recdir+'/'+fname+'.wav', 
                        imgdir+'/'+dir_date+'/'+satName+'/'+fileNameC+'-mcir.jpg']
        subprocess.call(cmdline_mcir)

#Creates a spectrum from recording
def spectrum(fname):
    # Changed spectrum generation, now it creates spectrogram from recorded WAV file
    # Optional
    print '\nCreating flight spectrum...'
    cmdline = ['sox',recdir+'/'+fname+'.wav', '-n', 'spectrogram','-o',specdir+'/'+fname+'.png']
    subprocess.call(cmdline)

#Executes pypredict to find next pass
def findNextPass():
    print "\nSeeking passes with minimum elevation of " +str(min_elev)+ " degrees..."
    predictions = [pypredict.aoslos(s, min_elev) for s in satellites]
    aoses = [p[0] for p in predictions]
    nextIndex = aoses.index(min(aoses))
    return (satellites[nextIndex],\
            freqs[nextIndex],\
            predictions[nextIndex]) 

#Main loop
try:
  while True:
      if debugging == "yes": #Debug mode
        print "\nWARNING: DEBUG MODE - WILL NOT RECORD ANYTHING\n"
        #Dummy values
        satName = "NOAA 19"
        freq = 137100000
        aosTime = 1473512813 
        losTime = 1473513762
        max_elev = 90
        min_elev = 0
        now = aosTime
      else: #Normal mode
        (satName, freq, (aosTime, losTime, max_elev, min_elev)) = findNextPass()
        print aosTime
        print losTime
        
        now = time.time()
      
      towait = aosTime-now
      aosTimeCnv=strftime('%H:%M:%S', time.localtime(aosTime))
      emergeTimeUtc=strftime('%Y-%m-%dT%H:%M:%S', time.gmtime(aosTime))
      losTimeCnv=strftime('%H:%M:%S', time.localtime(losTime))
      dimTimeUtc=strftime('%Y-%m-%dT%H:%M:%S', time.gmtime(losTime))
      if towait>0:
          print "\nNEXT PASS"
          print "Waiting for %s - expected AOS at %s" % (satName, aosTimeCnv)
          print "\tMaximum elevation is %s deg" % max_elev[0]
          
          #Viser info paa skjerm, for det er dritkult
          #date = time.strftime('%H:%M:%S')
          
          #nlcdx.write(1,date)
          #nlcdx.write(2,satName)
          #nlcdx.write(3,towait)
          #nlcdx.write(4,aosTimeCnv)
          #nlcdx.display()

          time.sleep(towait)

      # If the script broke and sat is passing by - change record time to reflect time change
      if debugging == "yes":
        recordTime = 5
      else:
        if aosTime<now:
          recordTime=losTime-now
        elif aosTime>=now:
          recordTime=losTime-aosTime
      
      # Go on, for now we'll name recordings and images by Unix timestamp.
      fname=str(aosTime)
      xfname=satName
      print "Beginning pass of "+satName+" - expected LOS at "+losTimeCnv 
      print "\tWill record for " +str(recordTime).split(".")[0]+" seconds."
      #Etter recordWAV maa vi ha en sjekk paa hvor lang fila endte opp med aa vaere. Sjekk "sox output.wav -n stat"
      recordWAV(freq,fname,recordTime)

      #Naar vi veit hvor lang fila er, maa vi sette flagget i wxmap som tilsier AOS + duration
      decode(fname,aosTime,satName) # make picture
      print "\nFinished pass of "+satName+" at "+losTimeCnv

      # Is this really needed?
      time.sleep(10.0)
except KeyboardInterrupt:
  print "\nExiting..."
